package generate_random_renge;

import java.util.Random;
import java.util.Scanner;

public class Process {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите порог диапазона: ");
        double thresholdRange = scanner.nextDouble();
        System.out.print("Введите коэффициент влияния порога(>1.0): ");
        double ratioThreshold = scanner.nextDouble();

        System.out.print("Введите максимальное время моделирования в секундах: ");
        int maxDelayTime = scanner.nextInt();

        long beginProcess = System.currentTimeMillis();

        double range = thresholdRange * ratioThreshold;//D+Dk
        double randomNumber;
        while (true) {
            randomNumber = generateRandomNumber(-Math.abs(range), Math.abs(range));
            if (randomNumber > thresholdRange) break;

            Thread.sleep(generateRandomTime(maxDelayTime)); // Случайно генерируемая задержка
        }

        long endProcess = System.currentTimeMillis();

        long timeProcess = endProcess - beginProcess;// Получаем время процесса

        System.out.println("Время от начала эксперимента: " + timeProcess + " миллисекунд."); //Выводим на время консоль
    }

    public static double generateRandomNumber(double bottom, double top) {// Метод генерирования случайных чисел
        double generateRandomNumber = (Math.random() * ((top - bottom) + 1)) + bottom;// Случайное число
        System.out.println(generateRandomNumber);// Вывод в консоль
        return generateRandomNumber;
    }

    public static long generateRandomTime(int maxTime) {// Метод генерирования случайного времени в диапазоне maxTime
        Random random = new Random();
        return random.nextInt(maxTime) * 1000L;
    }
}
